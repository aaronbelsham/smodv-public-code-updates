# This is code that is safe to be uploaded untouched to a smodv-public-code website.  It only contains updates to the core, which provided you haven't modified, won't kill your site.

# You actually only need to download this if you have removed the updater script.