<?php
/**
 * Created by PhpStorm.
 * User: jethro
 * Date: 23/01/2014
 * Time: 12:54 PM
 */

class template_image {
    public static function buildOutput($images) {
        $output = '
        <div id="images_list_id" class="images_list_container">';
        $output .= '
            <h5>Images</h5>
            <ul>';
        foreach ($images as $image){
            $output .= '
            <li>
            <img src="'.$image->paperclip_image_file_name.'"/>
            </li>';
        }
        $output .= '
            </ul>';

        $output .= '
        </div>';
        return $output;
    }
} 