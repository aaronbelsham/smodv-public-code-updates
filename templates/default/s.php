<?php
/**
 * Created by PhpStorm.
 * User: jethro
 * Date: 3/02/2014
 * Time: 1:23 PM
 */

class template_s {
    private $raw_page;
    public $output;
    private $mode;
    private $slug;
    private $slug1;
    private $slug2;
    private $slug3;
    private $slug4;
    private $plugins;

    public function __construct($page,$mode,$slug,$slug1,$slug2,$slug3,$slug4,$plugins){
        $this->plugins = $plugins;
        $this->page = $page;
        $this->raw_page = $page->raw_page;
        $this->page_results = $this->raw_page->page_results;
        $this->organisation_results = $this->raw_page->organisation_results;
        $this->group_results = $this->raw_page->group_results;
        $this->mode = $mode;
        $this->slug = $slug;
        $this->slug1 = $slug1;
        $this->slug2 = $slug2;
        $this->slug3 = $slug3;
        $this->slug4 = $slug4;
        $this->output = $this->buildHTML();
    }

    private function buildHTML(){
        $output = '
<div class="page_output">';
        $output .= '<p><h3>Search Results</h3></p>';
        //$output .= $this->raw_page->content.'
        $output .= '
    <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
            <a class="addthis_button_preferred_1"></a>
            <a class="addthis_button_preferred_2"></a>
            <a class="addthis_button_preferred_3"></a>
            <a class="addthis_button_preferred_4"></a>
            <a class="addthis_button_compact"></a>
            <a class="addthis_counter addthis_bubble_style"></a>
        </div>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515a9f1902a1a4b4"></script>
    <!-- AddThis Button END -->
        '.$this->buildSearchResults().'
</div>';
        return $output;
    }

    private function buildSearchResults() {

        $output = '
    <div id="search_results_id" class="search_results_container">';

        $output .= '<p><h4>Pages</h4></p>';
        $output .= '
        <div id="page_search_results_id" class="page_search_results_container">
            <ul>';
        foreach ($this->page_results as $page_result){
            $output .= '
                <li>
                    <p><a href="'.$page_result->full_url.'">'.$page_result->name.'</a><br />'.$page_result->description.'</p>
                </li>';
        }
        $output .= '
            </ul>
        </div>';


        $output .= '<p><h4>Organisations</h4></p>';
        $output .= '
        <div id="organisation_search_results_id" class="organisation_search_results_container">
            <ul>';
        foreach ($this->organisation_results as $organisation_result){
            $output .= '
                <li>
                    <p><a href="'.$organisation_result->full_url.'">'.$organisation_result->organisation_name.'</a><br />'.$organisation_result->description.'</p>
                </li>';
        }
        $output .= '
            </ul>
        </div>';


        $output .= '<p><h4>Groups</h4></p>';
        $output .= '
        <div id="group_search_results_id" class="group_search_results_container">
            <ul>';
        foreach ($this->group_results as $group_result){
            $output .= '
                <li>
                    <p><a href="'.$group_result->full_url.'">'.$group_result->name.'</a><br />'.$group_result->description.'</p>
                </li>';
        }
        $output .= '
            </ul>
        </div>';

        $output .= '
    </div>';

        return $output;
    }

    private function calculateSubItemURL($alias,$mode='p'){
        return  '/'.$mode.'/'.$alias;
    }

} 