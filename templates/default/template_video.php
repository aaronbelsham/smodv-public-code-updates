<?php
/**
 * Created by PhpStorm.
 * User: jethro
 * Date: 23/01/2014
 * Time: 12:55 PM
 */

class template_video {
    public static function buildOutput($videos) {
        $output = '
        <div id="videos_list_id" class="videos_list_container">';
        $output .= '
            <ul>';
        foreach ($videos as $video){
            $output .= '
            <li>
                <h6>'.$video->name.'</h6>
                <video controls="controls" width="320" height="240">
                    <source src="//'.$video->firefox_video_path.'" type="video/ogg">
                    <source src="//'.$video->web_video_path.'" type="video/mp4">
                </video>
                <p>'.$video->description.'</p>
            </li>';
        }
        $output .= '
            </ul>
        </div>';
        return $output;

    }
} 