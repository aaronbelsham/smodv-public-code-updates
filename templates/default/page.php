<?php
/*
 * This file creates a template for a page so you can customise its html output.
 * */

class PageTemplate {
    public $output;
    private $input;

    public function __construct($input) {
        $this->input = $input;
        $this->output = $this->buildOutput();
    }

    private function buildOutput() {
        $output = '
        <div class="page_header_row">
            <span class="created_on">Created at '.$this->input->created_on.'</span>
            <span class="created_by">by '.$this->input->created_by.'</span>
            <span class="created_on">Last modified at '.$this->input->created_on.'</span>
            <span class="created_by">by '.$this->input->modified_by.'</span>
        </div>
        ';
        $output .= '
        <div class="page_content_row">
        '.$this->input->content.'
        </div>
        ';
        return $output;
    }
}

?>