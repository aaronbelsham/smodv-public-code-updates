<?php
/**
 * Created by PhpStorm.
 * User: jethro
 * Date: 23/01/2014
 * Time: 12:53 PM
 */

class template_file {
    public static function buildOutput($files) {
        $output = '
        <div id="files_list_id" class="files_list_container">';
        $output .= '
            <h5>File Downloads</h5>
            <ul>';
        foreach ($files as $file){
            $output .= '
            <li>
                <a href="'.$file->paperclip_file_file_name.'">'.$file->paperclip_file_file_name.'</a>
            </li>';
        }
        $output .= '
            </ul>
        </div>';
        return $output;
    }
} 