<?php
/**
 * Created by PhpStorm.
 * User: jethro
 * Date: 23/01/2014
 * Time: 12:55 PM
 */

class template_website {
    public static function buildOutput($websites) {
        $output = '
        <div id="websites_list_id" class="websites_list_container">';
        $output .= '
            <h5>Website Links</h5>
            <ul>';
        foreach ($websites as $website){
            $output .= '
            <li>
                <p>'.$website->description.'<br />'.
                'Website : '.$website->web_address.'<br />'.
                '</p>
            </li>';
        }
        $output .= '
            </ul>';
        $output .= '
        </div>';
        return $output;
    }
}