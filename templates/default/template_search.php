<?php
/**
 * Created by PhpStorm.
 * User: jethro
 * Date: 3/02/2014
 * Time: 1:56 PM
 */

class template_search {
    public static function buildOutput() {
        $output = '
    <div id="search_id" class="search_container">';
        $output .= '
    <form name="input" action="../../search.php" method="post">
        <ul>
            <li>
                <input type="text" name="search_phrase">
            </li>
            <li>
                <input type="submit" name="submit" value="Search">
            </li>
        </ul>
    </form>';
        $output .= '
    </div>';
        return $output;
    }
}