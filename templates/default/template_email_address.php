<?php
/**
 * Created by PhpStorm.
 * User: jethro
 * Date: 23/01/2014
 * Time: 12:53 PM
 */

class template_email_address {
    public static function buildOutput($email_addresses) {
        $output = '
        <div id="emails_list_id" class="emails_list_container">';
        $output .= '
            <h5>Email Addresses</h5>
            <ul>';
        foreach ($email_addresses as $email){
            $emailText='Email';
            if ($email->description!='') $emailText = $email->description;
            $output .= '
            <li>
                <a href="mailto:'.$email->email_address.'">'.$emailText.'</a>
            </li>';
        }
        $output .= '
            </ul>
        </div>';
        return $output;
    }
} 