<?php
/**
 * Created by PhpStorm.
 * User: jethro
 * Date: 23/01/2014
 * Time: 12:51 PM
 */

class template_address {
    public static function buildOutput($addresses) {
        $output = '
        <div id="addresses_list_id" class="addresses_list_container">';
        $markers = '';
        $output .= '
            <ul>';
        foreach ($addresses as $address){
            $output .= '
            <li>
               <h5>Address</h5>'.
               '<h6>'.$address->description.'</h6>'.
                $address->line1.'<br />';
                $address->line2 == '' ?: $output.= '' | $output .= $address->line2.'<br />';
                $output .= $address->suburb.' '.$address->state.'<br />'.
                $address->suburb.'<br />'.
                $address->country.'<br />'.
                '</p>
            </li>';
            $markers .= '{
                        address: \''.$address->line1.' '.$address->line2.' '.$address->suburb.' '.$address->state.' '.$address->suburb.' '.$address->country.'\',
                        html: \'<h5>'.$address->description.'</h5><p>'.$address->line1.' '.$address->line2.' '.$address->suburb.' '.$address->state.' '.$address->suburb.' '.$address->country.'</p>\',
                        popup: false,
                        },';

        }
        $output .= '
            </ul>';
        $output.='
        </div>';
        $output.= '<div id="map_container">
        <script type=\'text/javascript\' src=\'http://maps.google.com/maps/api/js?sensor=true\'></script>
            <script>jQuery(document).ready(function($) {
                $(\'#gMapContact\').gMap({
                    controls: {
                    panControl: true,
                    zoomControl: true,
                    mapTypeControl: true,
                    scaleControl: true,
                    streetViewControl: true,
                    overviewMapControl: true
                },
                maptype: \'ROADMAP\',
                scrollwheel: false,
                zoom: 10,
                markers: [
                    '.$markers.'
                ]

            });
            });
            </script><div id="gMapContact"></div></p>';
        return $output;
    }
}