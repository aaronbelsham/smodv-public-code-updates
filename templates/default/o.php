<?php
/*
 * This file creates the page output.
 * In order to edit this, and NOT have it overwrite your existing code, please
 * make sure you copy all files into a folder with the name of the template you
 * want to call, e.g "custom" and then change the template variable in your index.php file.
 * All you need to do is edit the embedded html in the buildLayout() function.
 * */

class template_o {

    private $raw_page;
    public $output;
    private $mode;
    private $slug;
    private $slug1;
    private $slug2;
    private $slug3;
    private $slug4;
    private $plugins;

    public function __construct($page,$mode,$slug,$slug1,$slug2,$slug3,$slug4,$plugins){
        $this->plugins = $plugins;
        $this->page = $page;
        $this->raw_page = $page->raw_page;
        $this->mode = $mode;
        $this->slug = $slug;
        $this->slug1 = $slug1;
        $this->slug2 = $slug2;
        $this->slug3 = $slug3;
        $this->slug4 = $slug4;
        $this->output = $this->buildHTML();
    }

    private function buildHTML(){
        $output = '
<div class="page_output">';
        $output .= '<p><h3>'.$this->raw_page->organisation_name.'</h3></p>';
        $output .= count($this->page->images)>0 ? '<img src="'.$this->page->images[0]->paperclip_image_file_name.'"/>' : '';
        $output .= $this->raw_page->content.'
    <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
            <a class="addthis_button_preferred_1"></a>
            <a class="addthis_button_preferred_2"></a>
            <a class="addthis_button_preferred_3"></a>
            <a class="addthis_button_preferred_4"></a>
            <a class="addthis_button_compact"></a>
            <a class="addthis_counter addthis_bubble_style"></a>
        </div>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515a9f1902a1a4b4"></script>
    <!-- AddThis Button END -->
            '.template_phone::buildOutput($this->page->phones).'
            '.template_social::buildOutput($this->page->socials).'
            '.template_image::buildOutput($this->page->images).'
            '.template_email_address::buildOutput($this->page->email_addresses).'
            '.template_website::buildOutput($this->page->websites).'
            '.template_audio::buildOutput($this->page->audios).'
            '.template_file::buildOutput($this->page->files).'
            '.template_video::buildOutput($this->page->videos).'
            '.template_address::buildOutput($this->page->addresses).'
    <h6><em>Last updated '.$this->raw_page->updated_at.'</em></h6>
</div>';
        return $output;
    }

}