<?php
/**
 * Created by PhpStorm.
 * User: jethro
 * Date: 23/01/2014
 * Time: 12:52 PM
 */

class template_audio {
    public static function buildOutput($audios) {
        $output = '
        <div id="audios_list_id" class="audios_list_container">';
        $output .= '
            <ul>';
        foreach ($audios as $audio){
            $output .= '
            <li><h6>'.$audio->name.'</h6>
                <p>'.$audio->description.'</p>
                <audio controls="controls">
                    <source src="//'.$audio->mp3_audio_path.'" type="audio/mpeg">
                    <source src="//'.$audio->ogg_audio_path.'" type="audio/ogg">
                </audio>
            </li>';
        }
        $output .= '
            </ul>
        </div>';
        return $output;
    }
} 