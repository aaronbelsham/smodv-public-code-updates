<?php
/*
* This file creates the page output.
* In order to edit this, and NOT have it overwrite your existing code, please
* make sure you copy all files into a folder with the name of the template you
* want to call, e.g "custom" and then change the template variable in your index.php file.
* All you need to do is edit the embedded html in the buildLayout() function.
* */

class template_p {
    private $raw_page;
    private $sub_items;
    public $output;
    private $mode;
    private $slug;
    private $slug1;
    private $slug2;
    private $slug3;
    private $slug4;

    public function __construct($page,$mode,$slug,$slug1,$slug2,$slug3,$slug4){
        $this->page = $page;
        $this->raw_page = $page->raw_page->page;
        $this->sub_items = $page->sub_pages;
        $this->mode = $mode;
        $this->slug = $slug;
        $this->slug1 = $slug1;
        $this->slug2 = $slug2;
        $this->slug3 = $slug3;
        $this->slug4 = $slug4;
        $this->output = $this->buildHTML();
    }

    private function buildHTML(){
        $output = '
<div class="page_output">';
        $output .= str_replace('{READMORE}','',$this->raw_page->content).'
        <div>
            <!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
<a class="addthis_button_preferred_1"></a>
<a class="addthis_button_preferred_2"></a>
<a class="addthis_button_preferred_3"></a>
<a class="addthis_button_preferred_4"></a>
<a class="addthis_button_compact"></a>
<a class="addthis_counter addthis_bubble_style"></a>
</div>
<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5313181a22734502"></script>
<!-- AddThis Button END -->
            </div>
    <div class="sub_items">
        <ul class="sub_items_ul">
            '.$this->buildSubItemsList().'
        </ul>
    </div>
</div>';
        return $output;
    }

    private function buildSubItemsList(){
        $output = '';
        foreach($this->sub_items as $raw_page){
            $output .= '
<li class="sub_item_li">
    <h3>'.$raw_page->name.'</h3>';
            if(strpos($raw_page->page->content,'{READMORE}')){
                $output .= '<p>'.substr($raw_page->page->content,0,strpos($raw_page->page->content,'{READMORE}')).'</p>';
            }
            else{
                $output .= '<p>'.substr($raw_page->page->content,0,500).'</p>';
            }
            $output .= '
    <p><a class="btn btn-large btn-primary" href="'.$this->calculateSubItemURL($raw_page->page->alias,$this->raw_page->alias).'">Read More</a></p>
</li>
';
        }
        return $output;
    }

    private function calculateSubItemURL($alias,$parent){
        if($this->slug1 == ''){
            $url = '/p/'.$parent.'/'.$alias;
        }
        else if($this->slug2 == '' && $this->slug1 != ''){
            $url = '/p/'.$this->slug.'/'.$parent.'/'.$alias;
        }
        else if($this->slug3 == '' && $this->slug2 != ''){
            $url = '/p/'.$this->slug.'/'.$this->slug1.'/'.$parent.'/'.$alias;
        }
        else {
            $url = '/p/'.$this->slug.'/'.$this->slug1.'/'.$this->slug2.'/'.$parent.'/'.$alias;
        }
        return $url;
    }
}

?>