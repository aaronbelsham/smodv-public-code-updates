<?php
/**
 * Created by PhpStorm.
 * User: jethro
 * Date: 23/01/2014
 * Time: 12:55 PM
 */

class template_social {
    public static function buildOutput($socials) {
        $output = '
        <div id="socials_list_id" class="socials_list_container">';
        $output .= '
            <h5>Social Links</h5>
            <ul>';
        foreach ($socials as $social){
            $output .= '
            <li>
                <a href="//'.$social->social_link.'">'.ucfirst($social->social_network).'</a>
            </li>';
        }
        $output .= '
            </ul>
        </div>';
        return $output;
    }
} 